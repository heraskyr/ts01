package cz.cvut.fel.ts1.app.selenium;

import cz.cvut.fel.ts1.app.selenium.config.DriverFactory;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.yecht.Data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MapyCzPageTest {
    @Test
    public void fromBrnoToPrague() throws IOException {
        WebDriver driver = new DriverFactory().getDriver();
        MapyCzPage page = new MapyCzPage(driver);
        page.open();
        Route r = page.getRoute("Brno","Praha");
        System.out.println(r.getDistance());
        System.out.println(r.getTime().toString());
        //driver.quit();
    }

    @Test
    public void TSPMapyCzTest() throws IOException
    {
        TSPMapyCz tsp = new TSPMapyCz();
        List<String> cities = new ArrayList<>(Arrays.asList("Praha", "Brno","Liberec", "Ostrava"));

        tsp.computematrix(cities);
        long[][] m = tsp.getMatrix();
        for (long[] sl: m) {
            for (long i:
                 sl) {
                System.out.printf("%d ",i);
            }
            System.out.println();

        }
    }
}
