package cz.cvut.fel.ts1.app.controller;

import cz.cvut.fel.ts1.app.model.Request;
import cz.cvut.fel.ts1.app.model.Response;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class AppController {

    @PostMapping(
            value = "/api",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    Response getTestData(@RequestBody Request rq) {
        Response r = new Response();
        r.setName(rq.getQuestion());
        return r;
    }

}
