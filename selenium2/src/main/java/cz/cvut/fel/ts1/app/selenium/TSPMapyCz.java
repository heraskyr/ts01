package cz.cvut.fel.ts1.app.selenium;

import com.google.ortools.Loader;
import com.google.ortools.constraintsolver.*;
import cz.cvut.fel.ts1.app.TSP;
import cz.cvut.fel.ts1.app.selenium.config.DriverFactory;
import lombok.Getter;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Getter
public class TSPMapyCz
{
    private long[][] matrix;

    public long[][] getMatrix() {
        return matrix;
    }

    public void computematrix(List<String> cities) throws IOException {
        int len = cities.size();
        matrix = new long[len][len];
        for(int i = 0; i< cities.size(); i++)
        {
            for (int j = i; j < cities.size(); j++)
            {
                if(i == j)
                {
                    matrix[i][i] = 0;
                }
                else
                {
                    WebDriver driver = new DriverFactory().getDriver();
                    MapyCzPage page = new MapyCzPage(driver);
                    page.open();
                    Route r = page.getRoute(cities.get(i), cities.get(j));
                    matrix[i][j] = matrix[j][i] = r.getDistance();
                    driver.close();
                }

            }
        }
    }

    public static List<Long> getSolution(
            RoutingModel routing, RoutingIndexManager manager, Assignment solution)
    {
        // Solution cost.
        //logger.info("Objective: " + solution.objectiveValue() + "miles");
        // Inspect solution.
        //logger.info("Route:");
        List<Long> path = new ArrayList<>();
        long routeDistance = 0;
        String route = "";
        long index = routing.start(0);
        while (!routing.isEnd(index)) {
            path.add((long)manager.indexToNode(index));
            long previousIndex = index;
            index = solution.value(routing.nextVar(index));
            routeDistance += routing.getArcCostForVehicle(previousIndex, index, 0);
        }
        route += manager.indexToNode(routing.end(0));
        return path;
    }

     public void solveTSP()
     {
         Loader.loadNativeLibraries();
         // Instantiate the data problem.
         TSP.DataModel data = new TSP.DataModel();
         data.setDistanceMatrix(getMatrix());

         // Create Routing Index Manager
         RoutingIndexManager manager =
                 new RoutingIndexManager(data.distanceMatrix.length, data.vehicleNumber, data.depot);

         // Create Routing Model.
         RoutingModel routing = new RoutingModel(manager);

         // Create and register a transit callback.
         final int transitCallbackIndex =
                 routing.registerTransitCallback((long fromIndex, long toIndex) -> {
                     // Convert from routing variable Index to user NodeIndex.
                     int fromNode = manager.indexToNode(fromIndex);
                     int toNode = manager.indexToNode(toIndex);
                     return data.distanceMatrix[fromNode][toNode];
                 });

         // Define cost of each arc.
         routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);

         // Setting first solution heuristic.
         RoutingSearchParameters searchParameters =
                 main.defaultRoutingSearchParameters()
                         .toBuilder()
                         .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
                         .build();

         // Solve the problem.
         Assignment solution = routing.solveWithParameters(searchParameters);

         // Print solution on console.
         List<Long> = getSolution(routing, manager, solution);
     }
}
