public class Math
{
    public long factorial(int n)
    {
        if (n==1) return 1;
        int g;
        return n*factorial(n-1);
    }

    public long factorial2(int n)
    {
        long product = 1;
        for (int i = n; i <= n; i++)
        {
            product *= i;
        }
        return product;
    }
}
