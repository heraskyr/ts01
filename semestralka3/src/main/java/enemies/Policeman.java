package enemies;

import Entities.Entity;
import Entities.Player;
import Mechanics.CollisionChecker;
import Mechanics.GamePanel;
import Mechanics.Item;
import draw.ImageGetter;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Policeman extends Entity {
    private int actionLockCounter = 0;
    private static final int maxMemory = 1200;
    private int memory;
    private Player AgressiveAt;
    private int cooldown = 0;
    private static final int maxCooldown = 120;
    private static final int maxMessage = 120;
    private CollisionChecker cc;
    boolean alive = true;


    public Policeman() {
    }

    public Policeman(Entity e)
    {
        super(e);
        cc = new CollisionChecker(gp);
    }


    public void getImage()
    {
        down1 = ImageGetter.getImage("enemies/policeman_down1.png");
    }

    public void update()
    {
        if(alive) setAction();
    }

    public void punch()
    {
        if(near(gp.player, radiusAttack))
        {
            gp.player.health-=damage;
            gp.player.message = getHurtMsg();
            messageCounter = maxMessage;
            message = getAttackMsg();
         //   System.out.println("attacked");
        }
    }


    public void setAction() {
        xNext = x;
        yNext = y;
        if(alive && health<0)
        {
            for(Item item: items)
            {
                gp.tm.add(item,(int)x, (int)y);
            }
            messageCounter = 120;
            message = "died";
            alive = false;
        }
        if(memory>0)
        {
            if(cc.see(this, gp.player) && gp.player.seat)
            {
                memory-=3;
            }
            memory--;
        }


        if(memory<=0)
        {
            AgressiveAt = null;
            memory = 0;
        }
        if(cooldown > 0) cooldown--;



        if(near(gp.player, radiusAttack) && cooldown<=0)
        {
            punch();
            cooldown = maxCooldown;
            return;

        }

        if (!can_be_moved) {
            speed = 0;
        } else speed = 1;

        if(AgressiveAt==null && near(gp.player, 100))
        {

            AgressiveAt = gp.player;
            memory = maxMemory;
        }

        if(AgressiveAt != null && !near(gp.player, radiusAttack) && can_be_moved)
        {
            if(AgressiveAt.x > x && Math.abs(AgressiveAt.x - x) > radiusAttack)
            {
                xNext +=speed;
                //System.out.println("x+");
            }
            else if(AgressiveAt.x < x && Math.abs(AgressiveAt.x - x) > radiusAttack)
            {
                //System.out.println("x-");
                xNext -=speed;
            }
            else if(AgressiveAt.y > y)
            {
                yNext +=speed;
               // System.out.println("there");
            }
            else
            {
               // System.out.println("there2");
                yNext -=speed;
            }
        }
        else{
            actionLockCounter++;
            if (actionLockCounter == 100) {
                Random random = new Random();
                int i = random.nextInt(100) + 1;
                if (i <= 25) {

                    can_be_moved = false;
                    if (y > 80) {
                        can_be_moved = true;
                        if (can_be_moved) {
                            direction = "up";
                        }
                    }

                }
                if (i > 25 && i <= 50) {
                    can_be_moved = false;
                    if (y < 800 - 80) {
                        can_be_moved = true;
                        if (can_be_moved) {
                            direction = "down";
                        }
                    }
                }
                if (i > 50 && i <= 75) {
                    can_be_moved = false;
                    if (x > 80) {
                        can_be_moved = true;
                        if (can_be_moved) {
                            direction = "left";
                        }
                    }

                }
                if (i > 75 && i <= 100) {
                    can_be_moved = false;
                    if (x < 1920 - 80) {
                        can_be_moved = true;
                        if (can_be_moved) {
                            direction = "right";
                        }

                    }

                    direction = "up";
                }
                if (i > 25 && i <= 50) {
                    direction = "down";
                }
                if (i > 50 && i <= 75) {
                    direction = "left";
                }
                if (i > 75 && i <= 100) {
                    direction = "right";

                }
                actionLockCounter = 0;
            }
            if (direction == "up") {
                yNext = y - speed;
            }
            if (direction == "down") {
                yNext = y + speed;
            }
            if (direction == "left") {
                xNext = x - speed;
            }
            if (direction == "right") {
                xNext = x + speed;
            }
        }
        if(cc.checkTile(this)) {
            x = xNext;
            y = yNext;
        }
    }



    public void draw(Graphics2D g2, int deltax)
    {

        BufferedImage image = down1;
        g2.drawImage(image, Math.round(x)+deltax, Math.round(y), 64, 64, null);
        if(messageCounter>0)
        {
            messageCounter--;
            gp.ui.drawMessage(g2,this, message);
        }
    }



    }



