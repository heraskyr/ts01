package Mechanics;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.security.Key;

public class KeyHandler implements KeyListener {
    GamePanel gp;
    public boolean up;
    public boolean down;
    public boolean left;
    public boolean right;
    public boolean space;
    public boolean shift;

    public KeyHandler(GamePanel gp) {
        this.gp = gp;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e)
    {

        int code = e.getKeyCode();
        if(gp.gameState == GamePanel.runState) {
            switch (code) {
                case (KeyEvent.VK_SHIFT): {
                    shift = true;
                    break;
                }
                case (KeyEvent.VK_W): {
                    up = true;
                    break;
                }
                case (KeyEvent.VK_S): {
                    down = true;
                    break;
                }
                case (KeyEvent.VK_A): {
                    left = true;
                    break;
                }
                case (KeyEvent.VK_D): {
                    right = true;
                    break;
                }
                case (KeyEvent.VK_SPACE): {
                    space = true;
                    break;
                }
                case (KeyEvent.VK_Q):
                {
                    gp.player.throwOut();
                    break;
                }
                case (KeyEvent.VK_P):
                {
                    gp.pausegame();
                    break;
                }
            }
            if (e.getKeyCode() <= KeyEvent.VK_9 && e.getKeyCode() >= KeyEvent.VK_1)
            {
                //System.out.println(Character.getNumericValue(e.getKeyChar()));
                gp.player.selectItem(e.getKeyCode()-48);
            }
        }
        else if(gp.gameState == GamePanel.menuState || gp.gameState == GamePanel.loadState)
        {
            switch (e.getKeyCode()) {
                case (KeyEvent.VK_W): {
                    if (gp.ui.numberChosen==0) gp.ui.numberChosen=gp.ui.count-1;
                    else gp.ui.numberChosen= (gp.ui.numberChosen-1)%gp.ui.count;

                    break;
                }
                case (KeyEvent.VK_S): {
                    gp.ui.numberChosen= (gp.ui.numberChosen+1)%gp.ui.count;
                    break;
                }
                case (KeyEvent.VK_ENTER): {
                    gp.ui.submit();
                }
            }
        }
        else if (gp.gameState==GamePanel.pauseState) {
            switch (e.getKeyCode()) {
                case (KeyEvent.VK_W): {
                    if (gp.ui.numberChosen==0) gp.ui.numberChosen=2;
                    else gp.ui.numberChosen= (gp.ui.numberChosen-1)%3;

                    break;
                }
                case (KeyEvent.VK_S): {
                    gp.ui.numberChosen= (gp.ui.numberChosen+1)%3;
                    break;
                }
                case (KeyEvent.VK_ENTER): {
                    gp.ui.submit();
                    break;
                }
                case (KeyEvent.VK_ESCAPE):
                {
                    gp.continuegame();
                    break;
                }
                case (KeyEvent.VK_P):
                {
                    gp.continuegame();
                    break;
                }
            }
        }
        else if(gp.gameState == GamePanel.saveState)
        {
            switch (e.getKeyCode()) {
                case (KeyEvent.VK_W): {
                    if (gp.ui.numberChosen==0) gp.ui.numberChosen=gp.ui.count-1;
                    else gp.ui.numberChosen= (gp.ui.numberChosen-1)%gp.ui.count;

                    break;
                }
                case (KeyEvent.VK_S): {
                    gp.ui.numberChosen= (gp.ui.numberChosen+1)%gp.ui.count;
                    break;
                }
                case (KeyEvent.VK_ENTER): {
                    gp.ui.submit();
                }
                case (KeyEvent.VK_ESCAPE):
                {
                    gp.pausegame();
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int code = e.getKeyCode();
        switch (code)
        {
            case (KeyEvent.VK_SHIFT): {
                shift = false;
                break;
            }
            case(KeyEvent.VK_W):
            {
                up = false;
                break;
            }
            case(KeyEvent.VK_S):
            {
                down = false;
                break;
            }
            case(KeyEvent.VK_A):
            {
                left = false;
                break;
            }
            case(KeyEvent.VK_D):
            {
                right = false;
                break;
            }
            case(KeyEvent.VK_SPACE):
            {
                space = false;
                break;
            }
        }
    }
}
