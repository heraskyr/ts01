package Mechanics;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import Entities.Entity;
import Entities.Player;
import draw.ImageGetter;
import enemies.Policeman;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Parser
{
    public static ArrayList<TileParsed> getTiles(String filename, GamePanel gp)
    {
        ArrayList<TileParsed> tiles = new ArrayList<>();
        JSONParser jsonParser = new JSONParser();

            try
            {
                FileReader reader = new FileReader(filename);
                //Read JSON file
                Object obj = jsonParser.parse(reader);

                JSONArray tileList = (JSONArray)(((JSONObject) obj).get("tiles"));

                for (Object o: tileList) {
                    JSONObject o1 = (JSONObject)o;
                    String type = o1.get("type").toString();
                    TileParsed tp = new TileParsed(gp);
                    tp.type = type;
                    tp.src = o1.get("bg").toString();
                    tp.setImage(o1.get("bg").toString());
                    tp.collision = (boolean)o1.get("collision");
                    tp.canget = (boolean)o1.get("canget");
                    JSONArray arr = (JSONArray) o1.get("coords");
                    for(Object o2:arr)
                    {
                        int x = Integer.parseInt(((JSONObject)o2).get("x").toString());
                        int y = Integer.parseInt(((JSONObject)o2).get("y").toString());
                        if(tp.getClass() == Door.class) System.out.println(x+ " "+ y);
                        tp.addCoord(x, y);
                    }


                    if (type.equals("item"))
                    {
                        Item i = new Item (tp);
                        tiles.add(i);
                        System.out.println("There is Item");
                    }
                    else if(type.equals("weapon"))
                    {
                        Weapon w = new Weapon(tp);
                        w.damage = Integer.parseInt( o1.get("damage").toString());
                        w.radius = Integer.parseInt( o1.get("radius").toString());
                        tiles.add(w);
                        System.out.println("weapon");

                    }
                    else if (type.equals("key"))
                    {
                        gKey i = new gKey (tp);
                        tiles.add(i);
                        System.out.println("There is Key");
                    }
                    else if (type.equals("door"))
                    {
                        Door d = new Door(tp);
                        d.bg2 = o1.get("bg2").toString();
                        d.image2 = ImageGetter.getImage(o1.get("bg2").toString());
                        tiles.add(d);
                        System.out.println("Therre is door");
                    }
                    else{ tiles.add(tp);}


                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        return tiles;
    }

    public static ArrayList<Entity> parseEntities(String filename, GamePanel gp)
    {
        ArrayList<Entity> entities = new ArrayList<>();
        JSONParser jsonParser = new JSONParser();
        FileReader reader = null;
        try {
            reader = new FileReader(filename);
            Object obj = jsonParser.parse(reader);
            JSONArray tileList = (JSONArray)(((JSONObject) obj).get("entities"));
            for (Object o: tileList) {
                JSONObject o1 = (JSONObject) o;
                String type = o1.get("type").toString();



                int radius = Integer.parseInt (o1.get("radius").toString());
                int x = Integer.parseInt (o1.get("x").toString());
                int y = Integer.parseInt (o1.get("y").toString());


                JSONArray items = (JSONArray)o1.get("items");
                ArrayList<Item> items1 = new ArrayList<>();
                for(Object o2:items)
                {
                    Item item = new Item(gp);
                    JSONObject o3 = (JSONObject) o2;
                    item.type = o3.get("type").toString();
                    item.image = ImageGetter.getImage(o3.get("bg").toString());
                    item.src = o3.get("bg").toString();
                    if(item.type.equals("weapon"))
                    {
                        Weapon w = new Weapon(item);
                        w.damage = Integer.parseInt( o3.get("damage").toString());
                        w.radius = Integer.parseInt( o3.get("radius").toString());
                        items.add(w);
                        continue;
                    }
                    items1.add(item);
                }
                Entity e = new Entity(gp);
                e.type = type;
                e.x = x;
                e.y = y;
                e.items = items1;
                if(type.equals("player") || type.equals("police"))
                {
                    e.damage = Integer.parseInt (o1.get("damage").toString());
                }
                e.health =Integer.parseInt( o1.get("health").toString());
                e.radiusAttack = radius;

                e.strdown1 = o1.get("down1").toString();
                e.strdown2 = o1.get("down2").toString();
                e.strup1 = o1.get("up1").toString();
                e.strup2 = o1.get("up2").toString();
                e.strleft1 = o1.get("left1").toString();
                e.strleft2 = o1.get("left2").toString();
                e.strright1 = o1.get("right1").toString();
                e.strright2 = o1.get("right2").toString();
                
                
                e.down1 = ImageGetter.getImage(o1.get("down1").toString());
                e.down2 = ImageGetter.getImage(o1.get("down2").toString());
                e.up1 = ImageGetter.getImage(o1.get("up1").toString());
                e.up2 = ImageGetter.getImage(o1.get("up2").toString());
                e.left1 = ImageGetter.getImage(o1.get("left1").toString());
                e.left2 = ImageGetter.getImage(o1.get("left2").toString());
                e.right1 = ImageGetter.getImage(o1.get("right1").toString());
                e.right2 = ImageGetter.getImage(o1.get("right2").toString());
                if(e.type.equals("policeman"))
                {
                    Policeman p = new Policeman(e);
                    entities.add(p);
                    continue;
                }

                if(e.type.equals("player"))
                {
                    System.out.println("found player");
                    Player p = new Player(e);
                    p.radiusToGet = Integer.parseInt( o1.get("collectrad").toString());
                    gp.player = p;
                    continue;

                }


                entities.add(e);

            }
        return entities;



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveLevel(GamePanel gp, String filename)
    {
        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();
        if(gp.tm.tiles!=null) {
            System.out.println("is not null");
            for (TileParsed tile : gp.tm.tiles) {
                JSONObject o = new JSONObject();
                o.put("bg", tile.src);
                o.put("type", tile.type);
                o.put("collision", tile.collision);
                o.put("canget", tile.canget);
                if(tile.type.equals("weapon"))
                {

                    o.put("damage", ((Weapon)tile).damage);
                    o.put("radius", ((Weapon)tile).radius);
                }
                if(tile.type.equals("door"))
                {
                    o.put("bg2",((Door)tile).bg2);
                }
                JSONArray coords = new JSONArray();
                if (tile.coords != null) {
                    for (TileParsed.Coords c :tile.coords)
                    {
                        JSONObject coord = new JSONObject();
                        coord.put("x", c.x);
                        coord.put("y", c.y);
                        coords.add(coord);
                    }
                }
                o.put("coords", coords);
                //System.out.println(o);
                arr.add(o);
            }

        }
       obj.put("tiles", arr);

        arr = new JSONArray();
        ArrayList<Entity> entities = gp.getEntities();
        entities.add(gp.player);
        if(entities!=null)
        {
            for(Entity entity: entities)
            {
                JSONObject o = new JSONObject();
                o.put("type", entity.type);
                o.put("x", (int)entity.x);
                o.put("y", (int)entity.y);
                o.put("damage", entity.damage);
                o.put("radius", entity.radiusAttack);
                o.put("health", entity.health);
                if(entity.type.equals("player"))
                {
                    o.put("collectrad", (int)entity.radiusToGet);
                }
                JSONArray itemsJSON = new JSONArray();
                if(entity.items!=null) {
                    for (Item item : entity.items) {
                        JSONObject o1 = new JSONObject();
                        o1.put("type", item.type);
                        o1.put("bg", item.src);
                        if(item.type=="weapon")
                        {
                            o1.put("damage", ((Weapon)item).damage);
                            o1.put("radius", ((Weapon)item).radius);
                        }

                        
                        itemsJSON.add(o1);
                    }
                }
                o.put("down1", entity.strdown1);
                o.put("down2",entity.strdown2);
                o.put("up1",entity.strup1);
                o.put("up2",entity.strup2 );
                o.put("left1",entity.strleft1  );
                o.put("left2",entity.strleft2  );
                o.put("right1",entity.strright1  );
                o.put("right2",entity.strright2  );
                o.put("items",itemsJSON);
                arr.add(o);
            }
        }
        obj.put("entities", arr);
        FileWriter file = null;
        try {
            entities.remove(gp.player);
            file = new FileWriter(filename);
            file.write(obj.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}

