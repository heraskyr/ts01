package Mechanics;

import Entities.Entity;
import enemies.Policeman;

import java.awt.*;
import java.util.ArrayList;

public class EntityManager
{
    private ArrayList<Entity> entities;
    GamePanel gp;

    public EntityManager(GamePanel gp) {this.gp= gp;}
    public EntityManager(String level, GamePanel gp)
    {
        entities = Parser.parseEntities(level, gp);
    }

    public void update()
    {
        if (entities == null) return;
        for (Entity e: entities)
        {

            if (e.type.equals("policeman")) {((Policeman)e).update();continue;}
            e.update();
        }
    }

    public void draw(Graphics2D g2, int deltax)
    {
        if (entities == null) return;
        for (Entity e: entities)
        {
            if (e.type.equals("policeman")) {((Policeman)e).draw(g2,deltax);continue;}
            e.draw(g2, deltax);
        }
    }

    public void addEntity(Entity e)
    {
        if(entities==null) entities = new ArrayList<>();
        entities.add(e);
    }

    public ArrayList<Entity> getEntities()
    {
        return entities;
    }
}
