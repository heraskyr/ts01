package Mechanics;

import Entities.Entity;
import Entities.Player;
import draw.Tile;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Vector;

public class CollisionChecker
{
    private static class Vector2
    {
        int x;
        int y;

        public Vector2(int x1, int x2, int y1, int y2)
        {
            x = x1-x2;
            y = y1-y2;
        }

        public static float crs(Vector2 v1, Vector2 v2) {
            return v1.x * v2.y - v1.y * v2.x;
        }

        public Vector2 scl (float scalar) {
            x *= scalar;
            y *= scalar;
            return this;
        }

        public static boolean fromDifferentSides(Vector2 main, Vector2 v1, Vector2 v2){
            float product1 =crs(main ,v1), product2 = crs(main ,v2);
            return (product1>=0&&product2<=0 || product1<=0&&product2>=0);
        }

        public static boolean linesIntersect(int x1, int x2, int x3, int x4, int y1, int y2, int y3, int y4) {
            //vectors are x1-x2; x3-x4
            Vector2 main = new Vector2(x1, x2, y1, y2);
            Vector2 v1 = new Vector2(x1,x3,y1,y3);
            Vector2 v2 = new Vector2(x1,x4,y1,y4);

            if (fromDifferentSides(main,v1,v2)) {
                main = new Vector2(x3,x4,y3,y4);
                v1 =new Vector2(x3,x1,y3,y1);
                v2 =new Vector2(x3,x2,y3,y2);
                return fromDifferentSides(main,v1,v2);
            }
            return false;
        }
    }


    GamePanel gp;

    public CollisionChecker(GamePanel gp) {
        this.gp = gp;
    }




    synchronized public boolean see(Entity entity1, Entity entity2)
    {
        int ex1 = (int)entity1.x;
        int ey1 = (int)entity1.y;
        int ex2 = (int)entity2.x;
        int ey2 = (int)entity2.y;

        int vx1, vx2, vy1,vy2;


        if(gp.tm.tiles == null) return true;
        for(TileParsed tiletype: gp.tm.tiles)
        {
            for (TileParsed.Coords c:tiletype.coords)
            {

                //up left to up right
                vx1 = c.x;
                vx2 = c.x+tiletype.width;
                vy1 = c.y;
                vy2 = c.y;

                if (Vector2.linesIntersect(ex1, ex2, vx1, vx2, ey1, ey2, vy1, vy2)) return false;

                //up left to down left
                vx1 = c.x;
                vx2 = c.x;
                vy1 = c.y;
                vy2 = c.y+tiletype.height;
                if (Vector2.linesIntersect(ex1, ex2, vx1, vx2, ey1, ey2, vy1, vy2)) return false;

                //up right to down right
                vx1 = c.x + tiletype.width;
                vx2 = c.x+ tiletype.width;
                vy1 = c.y;
                vy2 = c.y+tiletype.height;
                if (Vector2.linesIntersect(ex1, ex2, vx1, vx2, ey1, ey2, vy1, vy2)) return false;


                //up left to up right
                vx1 = c.x;
                vx2 = c.x+tiletype.width;
                vy1 = c.y+tiletype.height;
                vy2 = c.y+tiletype.height;
                if (Vector2.linesIntersect(ex1, ex2, vx1, vx2, ey1, ey2, vy1, vy2)) return false;

            }
        }
        return  true;
    }

    synchronized public boolean checkTile(Entity entity)
    {

        int leftX = Math.round( entity.xNext) + 16 + 1;
        int rightX = Math.round(entity.xNext) + 16 + 16 - 1 ;
        int topY = Math.round(entity.yNext) + 16 + 1;
        int bottomY = Math.round(entity.yNext) + 16 + 16 - 1;

        if(!entity.direction.equals("up")) {topY+= entity.speed;}
        if(!entity.direction.equals("down")){bottomY-=entity.speed;}
        if(!entity.direction.equals("left")){leftX+=entity.speed;}
        if(!entity.direction.equals("right")){rightX-=entity.speed;}

        if(gp.tm.tiles == null) return true;
        for(TileParsed tiletype: gp.tm.tiles)
        {

            if(entity.getClass()==Player.class)//TBD method only for player
            {
                Player player = (Player) entity;
                if (player.getClass() == Player.class) {

                    ArrayList<TileParsed.Coords> toRemove = new ArrayList<>();
                    for (TileParsed.Coords c : tiletype.coords)
                {
                        if (Math.sqrt((c.x - player.x - 32) * (c.x - player.x - 32) + (c.y - player.y - 32) * (c.y - player.y - 32)) <=  player.radiusToGet) {

                            System.out.println("really close");
                            if(tiletype.getClass() == Item.class ||tiletype.getClass() == gKey.class  || tiletype.getClass() == Weapon.class) {
                                System.out.println("close to sth");
                                if(player.last < 10) {
                                    player.give((Item)tiletype);
                                    toRemove.add(c);
                                }
                            }

                        }
                        /*player.y < yTop && p(player, (xleft, yTop)) < radius ||
player.y > yTop && player.y < yBottom && (leftX - player.x) < radius ||
player.y > yBottom && p(player, (xleft, yBottom)) < radius ||
*/



                        if(tiletype.type.equals("door") && player.getSpacetyped() && !((Door)tiletype).opened && player.getItemSelected()!=null &&player.getItemSelected().type.equals("key") &&
                                ((player.y + 32 < c.y &&  ((c.x - player.x - 32) * (c.x - player.x - 32) + (c.y - player.y - 32) * (c.y - player.y - 32)) <= 25)
                                        || (player.y + 32 > c.y && player.y + 32 < (c.y + tiletype.height) && (c.x - player.x - 32) <= 20)
                                        ||(player.y + 32 < c.y && ((c.x - player.x - 32) * (c.x - player.x - 32) + (c.y + tiletype.height - player.y - 32) * (c.y+ tiletype.height - player.y - 32)) <= 25)))
                        {
                            System.out.println("a");
                           ((Door)tiletype).open();
                        }
                    }
                    for (TileParsed.Coords trc : toRemove) {
                        tiletype.coords.remove(trc);
                    }
                }

            }
            if(tiletype.collision==true) {
                for (TileParsed.Coords c:tiletype.coords)
                {
                    if((((
                            ( leftX>c.x && leftX<(c.x + tiletype.width) )||( rightX>c.x && rightX<(c.x + tiletype.width)  )
                    ) && (
                            ( topY>c.y && topY<(c.y + tiletype.height) )||( bottomY>c.y && bottomY<(c.y + tiletype.height) )
                    )
                    )||(
                            (
                                    ( c.x>leftX && c.x<rightX )||( (c.x + tiletype.width)>leftX && (c.x + tiletype.width)<rightX  )
                            ) && (
                                    ( c.y>topY && c.y<bottomY )||( (c.y + tiletype.height)>topY && (c.y + tiletype.height)<bottomY )
                            )
                    )
                    )||(
                            (
                                    (
                                            ( leftX>c.x && leftX<(c.x + tiletype.width) )||( rightX>c.x && rightX<(c.x + tiletype.width)  )
                                    ) && (
                                            ( c.y>topY && c.y<bottomY )||( (c.y + tiletype.height)>topY && (c.y + tiletype.height)<bottomY )
                                    )
                            )||(
                                    (
                                            ( c.x>leftX && c.x<rightX )||( (c.x + tiletype.width)>leftX && (c.x + tiletype.width)<rightX  )
                                    ) && (
                                            ( topY>c.y && topY<(c.y + tiletype.height) )||( bottomY>c.y && bottomY<(c.y + tiletype.height) )
                                    )
                            )
                    ))
                    {
                        /*System.out.println("LeftX " + leftX +" < " + (c.x + tiletype.width));
                        System.out.println("rightX " + rightX + " > " + c.x);
                        System.out.println("topY " + topY + " < " + (c.y + tiletype.height));
                        System.out.println("bottomY " + bottomY + " > "+  c.y);*/
                        return false;
                    }

                /*if(leftX < (c.x + tiletype.width) || rightX > c.x || topY < (c.y + tiletype.height) || bottomY > c.y)
                {
                    System.out.println("LeftX " + leftX +" < " + (c.x + tiletype.width));
                    System.out.println("rightX " + rightX + " > " + c.x);
                    System.out.println("topY " + topY + " < " + (c.y + tiletype.height));
                    System.out.println("bottomY " + bottomY + " > "+  c.y);
                    return false;
                }*/
                }}
        }
        //System.out.println("Ok");
        return true;
    }
}
