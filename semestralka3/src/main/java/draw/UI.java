package draw;

import Entities.Entity;
import Entities.Player;
import Mechanics.GamePanel;
import Mechanics.HealthIcon;
import Mechanics.Item;
import Mechanics.gKey;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.InputMismatchException;

public class UI {
    GamePanel gp;
    Font font;
    Graphics2D g2;
    public int numberChosen;
    public int count = 3;
    HealthIcon healthIco = new HealthIcon(GamePanel.healthSrc, GamePanel.healthSrc1);

    public UI(GamePanel gp) {
        this.gp = gp;

        InputStream is = getClass().getResourceAsStream("/font/VT323-Regular.ttf");
        InputStream is2 = getClass().getResourceAsStream("/font/Macondo-Regular.ttf");
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(Font.PLAIN, 32F);

            System.out.println("Font found");
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void draw(Graphics2D g2) {
       // System.out.println("Got here with state" + gp.gameState);
        this.g2 = g2;
        /*g2.setFont(font);
        g2.setColor(Color.black);
        g2.drawString("Key = " + gp.player.isHaskey(), 50, 50);
*/
        if (gp.gameState == GamePanel.menuState) {
            drawTitle();
        }
        else if (gp.gameState==GamePanel.runState)
        {
            healthIco.draw(g2, gp.player.getHealth());
            drawInventory(g2);
        }
        else if (gp.gameState==GamePanel.loadState)
        {
            drawlevels();
        }
        else if(gp.gameState==GamePanel.pauseState)
        {
            count = 3;
            drawpause();
        }
        else if(gp.gameState == GamePanel.saveState)
        {
            //System.out.println("got here");
            drawlevels();
        }
    }


    private void drawpause()
    {
        g2.setFont(font.deriveFont(Font.PLAIN, 96F));
        g2.setColor(Color.white);
        String text = "Pause";
        int x = centeredText(text);
        int y = gp.tilesize * 4;
        g2.drawString(text, x, y);

        if (numberChosen == 0) {
            printRed("Continue", y);
            y += gp.tilesize;
            printWhite("Save", y);
            y+=gp.tilesize;
            printWhite("Exit", y);
        } else if(numberChosen == 1){
            printWhite("Continue", y);
            y += gp.tilesize;
            printRed("Save", y);
            y+=gp.tilesize;
            printWhite("Exit", y);
        }
        else
        {
            printWhite("Continue", y);
            y += gp.tilesize;
            printWhite("Save", y);
            y+=gp.tilesize;
            printRed("Exit", y);
        }
    }


    private String getExtension(String filename)
    {
        String extension = "";

        int i = filename.lastIndexOf('.');
        if (i > 0) {
            extension = filename.substring(i+1);
        }
        return extension;
    }

    private void drawlevels()
    {
        count = 0;
        File folder = new File("./src/main/resources/levels");
        for (File fileEntry : folder.listFiles())
        {
            if(getExtension(fileEntry.getName()).equals("json"))
            {
                count++;
            }
        }
        //System.out.println(count);
        int i = 0;
        int y = gp.tilesize*4;
        for(File fileEntry : folder.listFiles())
        {
            //System.out.println(numberChosen);
            String name = fileEntry.getName();
            if(getExtension(fileEntry.getName()).equals("json")) {
                if(fileEntry.length()==0)name+=" - empty";
                if (i == numberChosen) {
                    printRed(name, y);
                } else printWhite(name, y);
                y += gp.tilesize;
                i++;
            }
        }
    }

    private void drawTitle() {
        g2.setFont(font.deriveFont(Font.PLAIN, 96F));
        g2.setColor(Color.white);
        String text = "Metro discoverer";
        int x = centeredText(text);
        int y = gp.tilesize * 4;
        g2.drawString(text, x, y);

        if (numberChosen == 0) {
            printRed("start", y);
            y += gp.tilesize;
            printWhite("load", y);
            y+=gp.tilesize;
            printWhite("finish", y);
        } else if(numberChosen == 1){
            printWhite("start", y);
            y += gp.tilesize;
            printRed("load", y);
            y+=gp.tilesize;
            printWhite("finish", y);
        }
        else
        {
            printWhite("start", y);
            y += gp.tilesize;
            printWhite("load", y);
            y+=gp.tilesize;
            printRed("finish", y);
        }
    }

    void printRed(String text, int y) {
        g2.setColor(Color.RED);
        g2.setFont(font.deriveFont(Font.PLAIN, 72F));
        int x = centeredText(text);
        y += gp.tilesize * 3;
        g2.drawString(text, x, y);
        g2.drawString(">", x - gp.tilesize, y);
        g2.setColor(Color.WHITE);
    }

    private void printWhite(String text, int y) {
        g2.setColor(Color.white);
        g2.setFont(font.deriveFont(Font.PLAIN, 72F));
        int x = centeredText(text);
        y += gp.tilesize * 3;
        g2.drawString(text, x, y);
    }

    private int centeredText(String s) {
        int len = (int) g2.getFontMetrics().getStringBounds(s, g2).getWidth();
        return gp.getWidth() / 2 - len / 2;
    }

    public void submit()
    {
        if(gp.gameState==GamePanel.menuState) {
            if (numberChosen == 0) {
                gp.parselevel("./src/main/resources/levels/level.json");
                gp.continuegame();
            } else if (numberChosen == 1) {
                gp.loadgame();
                numberChosen = 0;

            } else {
                gp.stopGame();
                //System.exit(-1);
            }
        }
        else if(gp.gameState==GamePanel.loadState) {
            int i = 0;

            File folder = new File("./src/main/resources/levels");
            for (File fileEntry : folder.listFiles()) {
                if (getExtension(fileEntry.getName()).equals("json"))
                {
                    if(i==numberChosen)
                    {
                        gp.parselevel("./src/main/resources/levels/"+fileEntry.getName());
                        gp.gameState = GamePanel.runState;
                        System.out.println("Choosed " + fileEntry.getName());
                        return;
                    }
                    i++;
                }
            }
        }
        else if(gp.gameState == GamePanel.pauseState)
        {
            if(numberChosen==0)
            {
                gp.continuegame();
            }
            else if (numberChosen == 2)
            {
                gp.stopGame();
                //System.exit(1);
            }
            else if(numberChosen == 1)
            {
                System.out.println("here");
                gp.savegame();
            }
        }
        else if(gp.gameState==GamePanel.saveState) {
            int i = 0;

            File folder = new File("./src/main/resources/levels");
            for (File fileEntry : folder.listFiles()) {
                if (getExtension(fileEntry.getName()).equals("json"))
                {
                    if(i==numberChosen)
                    {
                        gp.savelevel("./src/main/resources/levels/"+fileEntry.getName());
                        gp.gameState = GamePanel.pauseState;
                        System.out.println("Choosed " + fileEntry.getName());
                        return;
                    }
                    i++;
                }
            }
        }
    }

    public void drawInventory(Graphics2D g2)
    {
        int x = 20;
        int y = 60;
        for(int i = 0; i < gp.player.items.size(); i ++)
        {
            {
                g2.setColor(Color.gray);
                g2.fillRect(x,y,30, 30);
            }
            if(gp.player.items.get(i)!=null)
            {
                g2.drawImage(gp.player.items.get(i).getImage(), x, y, 30, 30, null);
            }
            y+=33;
        }
        Item selected = gp.player.getItemSelected();
        if(selected!=null)
        {
            g2.drawImage(selected.getImage(), 1450, 30, 40, 40, null);
        }
    }

    public void drawMessage(Graphics2D g2, Entity e, String message)
    {
        g2.setFont(font.deriveFont(Font.PLAIN, 36F));
        g2.setColor(Color.RED);
        g2.drawString(message, e.x-5 + gp.deltax, e.y-10);
    }

}
