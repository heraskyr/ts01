package Entities;

import Mechanics.GamePanel;
import Mechanics.Item;
import Mechanics.TileParsed;
import draw.ImageGetter;


import java.awt.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Entity
{
    public float x = 500;
    public float y = 500;
    public int speed = 4;
    public boolean can_be_moved;
    public int attack_speed;
    public String direction = "left";
    public String type;
    public double radiusToGet;
    public int radiusAttack;
    public ArrayList<Item> items;
    public BufferedImage up1, up2, down1, down2, left1, left2, right1, right2;
    public float xNext = 500;
    public float yNext = 500;
    public int spriteCounter = 0;
    public int spriteNum;
    public GamePanel gp;
    public int health;
    public int damage;
    public Rectangle solidArea;
    public boolean collision_on = false;
    public int messageCounter = 0;
    public static final int maxMessage = 120;
    public String message = "bonk";
    public static ArrayList<String> hurtMessage;
    public static ArrayList<String> attackMessage;
    private static java.util.Random random = new java.util.Random();
    public String strup1, strup2,strdown1, strdown2, strleft1, strleft2, strright1, strright2;

    public static void initMSG()
    {
        hurtMessage = new ArrayList<>();
        attackMessage = new ArrayList<>();
        hurtMessage.add("Ouch");
        hurtMessage.add("It hurts");
        hurtMessage.add("why");
        hurtMessage.add(":(");
        hurtMessage.add("I will die here");
        attackMessage.add("Punch");
        attackMessage.add("Bonk");
        attackMessage.add("Boom");
        attackMessage.add("pew");

    }

    public static String getAttackMsg()
    {
        if (attackMessage==null)return "";
        return attackMessage.get(random.nextInt(attackMessage.size()));
    }

    public static String getHurtMsg()
    {
        if(hurtMessage==null)return "";
        return hurtMessage.get(random.nextInt(hurtMessage.size()));
    }

    public int getHealth() {
        return health;
    }

    public Entity()
    {}
    
    public Entity(GamePanel gp)
    {
        this.gp = gp;
    }

    public Entity(Entity e)
    {
        this.gp = e.gp;
        this.type = e.type;
        this.x = e.x;
        this.y = e.y;
        this.items = e.items;
        this.damage = e.damage;
        this.radiusAttack = e.radiusAttack;
        this.up1 = e.up1;
        this.up2 = e.up2;
        this.left1 = e.left1;
        this.left2 = e.left2;
        this.right1 = e.right1;
        this.right2 = e.right2;
        this.health = e.health;
        this.down1 = e.down1;
        this.down2 = e.down2;
        

        this.strup1 = e.strup1;
        this.strup2 = e.strup2;
        this.strleft1 = e.strleft1;
        this.strleft2 = e.strleft2;
        this.strright1 = e.strright1;
        this.strright2 = e.strright2;
        this.strdown1 = e.strdown1;
        this.strdown2 = e.strdown2;

    }
    

    public void give(Item tile){

    }

    public void draw(Graphics2D g2, int deltax)
    {

        BufferedImage image = down1;
        //gp.ui.drawMessage(g2,this, "Ouch.....");
        g2.drawImage(image, Math.round(x)+deltax, Math.round(y), 64, 64, null);

    }



    public void update()
    {

    }




    public boolean near(Entity p, int radius)
    {
        return ( Math.sqrt((p.x-x)*(p.x-x)+(p.y-y)*(p.y-y)) <= radius);
    }
}
