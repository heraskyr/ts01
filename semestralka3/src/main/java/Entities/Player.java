package Entities;

import Mechanics.*;
import draw.ImageGetter;

import enemies.Policeman;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Player extends Entity
{
   /* public static Logger LOGGER;
    static {
        try(FileInputStream ins = new FileInputStream("./src/main/java/Logging/log.properties")){
            LogManager.getLogManager().readConfiguration(ins);
            LOGGER = Logger.getLogger(Main.class.getName());
        }catch (Exception ignore){
            ignore.printStackTrace();
        }
    }*/


    KeyHandler keyh;

    public int deltax, deltaxNext;
    boolean collision;
    private boolean haskey = false;
    public int damage = 3;
    public int radius;
    public Item ItemSelected;
    public int selected;

    public Item[] inventory = new Item[10];
    public int last = 0;
    private static final int defaultRadius = 20;
    private static final int defaultDamage = 4;
    public boolean invincible;
    private int invincibleCounter = 0;
    private int cooldown=0;
    private int maxCooldown = 100;
    public boolean seat = false;
    int height;



    public Player(Entity e)
    {
        super(e);
    }

    public void setKeyh(KeyHandler keyh) {
        this.keyh = keyh;
    }

    public Player(GamePanel gp, KeyHandler keyh) {
        this.items = new ArrayList<>();
        this.gp = gp;
        this.keyh = keyh;
        this.radiusToGet = 20;



        health = 19;
        getImage();
        height = up1.getHeight();
        solidArea = new Rectangle(0, 0, 16,16);


    }

    public boolean getSpacetyped()
    {
        return keyh.space;
    }

    public void getImage()
    {
            up1 = ImageGetter.getImage("entities/up1.png");
            up2 = ImageGetter.getImage("entities/up2.png");
            down1 = ImageGetter.getImage("entities/down1.png");
            down2 = ImageGetter.getImage("entities/down2.png");
            left1 = ImageGetter.getImage("entities/left1.png");
            left2 = ImageGetter.getImage("entities/left2.png");
            right1 = ImageGetter.getImage("entities/right1.png");
            right2 = ImageGetter.getImage("entities/right2.png");
    }

    public void give(Item item)
    {
        //int index = getFirstNull();
        if(items.size()<10) {
            if (item.type.equals("magnet"))
            {
                this.radiusToGet *= 2;
            }
            if (item.type.equals("key"))
            {
                System.out.println("Player has key");
                haskey = true;
            }
            items.add(item);

            //inventory[index] = item;
        }
    }

    public void throwOut()
    {
        if(ItemSelected!=null)
        {

            gp.tm.add(ItemSelected, (int)x+20, (int)y);
            ItemSelected = null;
            items.remove(selected);
            //inventory[selected] = null;
        }
    }

    public boolean isHaskey() {
        return haskey;
    }

    public void selectItem(int num)
    {
        System.out.println(num);
        selected = num-1;
        if(num > items.size())
        {
            selected = -1;
            ItemSelected = null;
            return;
        }
        ItemSelected = items.get(num-1);//inventory[num-1];

        if(items.get(num-1)!=null && items.get(num-1).getClass() == Weapon.class)
        {
            damage = ((Weapon)ItemSelected).damage;
            radius = ((Weapon)ItemSelected).radius;
        }
        else
        {
            damage = defaultDamage;
            radius = defaultRadius;
        }
        System.out.println("Selected "+num);
    }

    public void contactPoliceman(Policeman policeman)
    {
       // if(x < policeman.x + policeman.radiusAttack && x > policeman.x - policeman.radiusAttack && y < policeman.y + policeman.radiusAttack && y > policeman.y - policeman.radiusAttack)
        //{
                health = health - policeman.damage;
                //System.out.println("Damaged. Health = " + health);
                invincible = true;
                message = "ouch";
                messageCounter = maxMessage;
        //}
    }


    /*public void update() {
        if (invincible) {
            invincibleCounter++;
            if (invincibleCounter > 60) {
                invincible = false;
                invincibleCounter = 0;
            }


        }
    }*/


    public void update() {
        if (x > 1940) {
            gp.win();
        }
        int localspeed = speed;
        if (keyh.shift) {
            localspeed /= 2;
            seat = true;
        } else {
            seat = false;
        }

        if (cooldown > 0) cooldown--;
        synchronized (this) {
            if (keyh.space && cooldown <= 0) {
                attack();
            }

            if (keyh.right || keyh.up || keyh.left || keyh.down) spriteCounter++;
            if (keyh.right && keyh.up) {
                moveUpRight(localspeed);
            } else if (keyh.up && keyh.left) {
                moveUpLeft(localspeed);
            } else if (keyh.left && keyh.down) {
                moveDownLeft(localspeed);
            } else if (keyh.down && keyh.right) {
                moveDownRight(localspeed);
            } else if (keyh.right) {
                moveRight(localspeed);
            } else if (keyh.up) {
                moveUp(localspeed);
            } else if (keyh.left) {
                moveLeft(localspeed);
            } else if (keyh.down) {
                moveDown(localspeed);
            }

            if (spriteCounter > 10) {
                spriteCounter = 0;
                spriteNum = spriteNum == 1 ? 2 : 1;
            }


            if (gp.cc.checkTile(this)) {
                y = yNext;
                x = xNext;
                deltax = deltaxNext;
            }
        }
    }

    void moveUpRight(int localspeed)
    {
        direction = "up";
        if(y>0 &&  (x < 1920-64) || (y<430&&y>300)) {
            if(deltax > -380 && x >= 1200)
            {
                deltaxNext=Math.round(deltax-localspeed/(1.4f));
                xNext = x + localspeed/(1.4f) ;
                yNext= y - localspeed/(1.4f);
            }
            else {
                xNext = x + localspeed / (1.4f);
                yNext = y - localspeed / (1.4f);
            }
        }
        else if(y>0){
            yNext = y - localspeed;
            xNext = x;
        }
        else if(x<1920-64) {
            if(deltax > -380 && x >= 1200)
            {
                deltaxNext-= deltax - localspeed;
                xNext = x;
                yNext = y;
            }
            else {
                xNext = x + localspeed;
                yNext = y;
            }
        }
    }

    public void attack()
    {
        if(gp.getEntities()!=null);
        for(Entity e : gp.getEntities())
        {
            if(near(e, 100))
            {
                //LOGGER.log(Level.INFO,"Player is attacking");
                //LOGGER.log(Level.INFO,"Mob health = " + e.health);
                //System.out.println("attack!!");
                message = getAttackMsg();
                e.message = getHurtMsg();
                e.health-=damage;
                messageCounter = maxMessage;
                e.messageCounter = e.maxMessage;
            }
        }
        cooldown = maxCooldown;
    }

    void moveUpLeft(int localspeed)
    {
        direction = "up";
        if(y<800-64 && x > 0 )
        {
            if(deltax < 0 && x < 800)
            {
                deltaxNext=Math.round(deltax + localspeed/(1.4f));
                xNext = x - localspeed/(1.4f);
                yNext = y - localspeed/(1.4f);
            }
            else {
                xNext = x - localspeed / (1.4f);
                yNext = y - localspeed / (1.4f);
            }
        }
        else if(y<800-64)
        {
            xNext = x;
            yNext= y - localspeed;
        }
        else if(x>0) if(deltax<0 && x < 800)
        {
            deltaxNext= deltax + localspeed;
            xNext = x - localspeed;
            yNext = y;
        }
        else
        {
            xNext = x - localspeed;
            yNext = y;
            deltaxNext = deltax;
        }
    }

    void moveDownRight(int localspeed)
    {
        direction = "down";
        if(y<800-64 && x < 1920-64 || (y<430&&y>300)) {
            if(deltax > -380 && x >= 1200)
            {
                deltaxNext=Math.round(deltax-localspeed/(1.4f));
                xNext = x + localspeed/(1.4f);
                yNext = y + localspeed / (1.4f);
            }
            else {
                xNext = x + localspeed / (1.4f);
                yNext = y + localspeed / (1.4f);
            }
        }
        else if(y<800-64) yNext= y + localspeed;
        else if(x<1920-64) {
            if(deltax > -380 && x >= 1200)
            {
                deltaxNext = deltax - localspeed;
                xNext = x + localspeed;
            }
            else xNext=x+localspeed;
        }
    }

    void moveDownLeft(int localspeed)
    {
        direction = "down";
        if(y<800-64 && x > 0 )
        {
            if(deltax<0 && x < 300 - deltax)
            {
                deltaxNext=Math.round(deltax + localspeed/(1.4f));
                xNext = x - localspeed/(1.4f);
                yNext= y + localspeed/(1.4f);
            }
            else {
                xNext = x - localspeed / (1.4f);
                yNext = y + localspeed / (1.4f);
            }
        }
        else if(y<800-64)
        {
            yNext=y + localspeed;
        }
        else if(x>0) if(deltax<0 && x < 300 - deltax)
        {
            deltaxNext=deltax + localspeed;
            xNext = x - localspeed;
        }
        else xNext=x-localspeed;
    }
    
    
    void moveLeft(int localspeed)
    {
        direction = "left";
        if(x>0)
        {
            if(deltax<0 && x < 300 - deltax)
            {
                deltaxNext=deltax+localspeed;
            }
            xNext=x-localspeed;
        }
    }

    void moveRight(int localspeed)
    {
        direction = "right";
        if(x<1920-64 || (y<430&&y>300))
        {
            if(deltax > -380 && x >= 1200)
            {
                deltaxNext= deltax-localspeed;

            }
            xNext= x +localspeed;
        }
    }

    void moveUp(int localspeed)
    {
        direction = "up";
        if(y>0) yNext = y-localspeed;
    }

    void moveDown(int localspeed)
    {direction = "down";
        if(y<800-64) yNext = y + localspeed;}


    public void draw(Graphics2D g2)
    {

        BufferedImage image = null;
        switch (direction)
        {
            case("up"): image = (spriteNum==1)?up1:up2; break;
            case("left"): image = (spriteNum==1)?left1:left2; break;
            case("down"): image = (spriteNum==1)?down1:down2; break;
            case("right"): image = (spriteNum==1)?right1:right2; break;
            default: image = left1;
            System.out.println("coords" + x+ " " + y);
        }
        if(messageCounter > 0)
        {
            messageCounter--;
            gp.ui.drawMessage(g2,this, message);
        }

        if(seat){
            height = 32;
            //System.out.println("seat");
        }
        else
        {
            height = 64;
        }
        g2.drawImage(image, Math.round(x + deltax), Math.round(y)+64-height, 64, height , null);

    }

    public Item getItemSelected() {
        return ItemSelected;
    }

    public int getFirstNull()
    {
        for (int i = 0; i < 10; i++)
        {
            if(items.get(i)==null)return i;
            //if(inventory[i]==null) return i;
        }
        return -1;
    }

}
