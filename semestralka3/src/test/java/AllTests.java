import Entities.Entity;
import Entities.Player;
import Mechanics.*;
import draw.ImageGetter;
import enemies.Policeman;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import static org.mockito.Mockito.mock;

public class AllTests
{
    @Test
    public void PlayerDamageTest()
    {
        GamePanel gp = new GamePanel();
        Player p = new Player(gp, mock(KeyHandler.class));
        p.health = 20;
        Policeman police = new Policeman(new Entity());
        police.damage = 4;
        p.contactPoliceman(police);
        Assertions.assertEquals(16,p.health);
    }

    @Test
    public void GivePlayerTest()
    {
        GamePanel gp = new GamePanel();
        Player p = new Player(gp, mock(KeyHandler.class));
        Item i = mock(Item.class);
        i.type = "key";
        p.give(i);
        ArrayList<Item> items = new ArrayList<>();
        items.add(i);
        Assertions.assertEquals(items, p.items);
    }

    @Test
    public void GivePlayerTest_fullInventory()
    {
        GamePanel gp = new GamePanel();
        Player p = new Player(gp, mock(KeyHandler.class));
        Item item = mock(Item.class);
        item.type = "key";
        ArrayList<Item> items = new ArrayList<>();
        for(int i = 0; i < 10; i++)
        {
            items.add(item);
        }
        p.items = items;
        p.give(item);
        Assertions.assertEquals(items, p.items);
    }

    @Test
    public void ImageTest()
    {
        String res = "entities/down1.png";
        try {
            BufferedImage image = ImageIO.read(getClass().getResourceAsStream("/"+res));
            Assertions.assertTrue(ImageGetter.compareImages(image, ImageGetter.getImage(res)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void openDoorTest()
    {
        Door d = new Door(mock(GamePanel.class));
        d.image2 = mock(BufferedImage.class);
        d.coords.add(new TileParsed.Coords(0,0));
        d.open();
        Assertions.assertEquals(d.getImage(), d.image2);
    }

    @Test
    public void giveMagnetTest()
    {
        GamePanel gp = new GamePanel();
        Player p = new Player(gp, mock(KeyHandler.class));
        p.radiusToGet = 3;
        Item i = mock(Item.class);
        i.type = "magnet";
        p.give(i);
        Assertions.assertEquals(6, p.radiusToGet);
    }

    @Test
    public void processTest1()
    {
        int menustate;
        GamePanel gp = new GamePanel();
        gp.startGameThread();
        menustate = gp.gameState;
        gp.ui.numberChosen = 2;
        gp.ui.submit();
        Assertions.assertEquals(GamePanel.menuState, menustate);
    }

    @Test
    public void processTest2()
    {
        int menustate, gamestate, pausestate;
        GamePanel gp = new GamePanel();
        gp.startGameThread();
        menustate = gp.gameState;
        gp.ui.numberChosen = 0;
        gp.ui.submit();
        gamestate = gp.gameState;
        gp.pausegame();
        pausestate = gp.gameState;
        gp.ui.numberChosen = 2;
        gp.ui.submit();

        Assertions.assertEquals(GamePanel.menuState, menustate);
        Assertions.assertEquals(GamePanel.runState, gamestate);
        Assertions.assertEquals(GamePanel.pauseState, pausestate);

    }

    @Test
    public void processTest3()
    {
        int menustate, gamestate, pausestate, loadstate;
        GamePanel gp = new GamePanel();
        gp.startGameThread();
        menustate = gp.gameState;
        gp.ui.numberChosen = 1;
        gp.ui.submit();
        loadstate = gp.gameState;
        gp.ui.numberChosen=0;
        gp.ui.submit();
        gamestate = gp.gameState;
        gp.pausegame();
        pausestate = gp.gameState;
        gp.ui.numberChosen = 2;
        gp.ui.submit();

        Assertions.assertEquals(GamePanel.menuState, menustate);
        Assertions.assertEquals(GamePanel.runState, gamestate);
        Assertions.assertEquals(GamePanel.pauseState, pausestate);
        Assertions.assertEquals(GamePanel.loadState, loadstate);
    }

    @Test
    public void IntegratedDamageTest()
    {
        GamePanel gp = new GamePanel();
        gp.em = new EntityManager(gp);
        Policeman police = new Policeman();
        Player p = new Player(gp, mock(KeyHandler.class));
        p.x = 10;
        p.y= 10;
        p.radiusAttack = 10;
        police.y = 5;
        police.x = 5;
        police.health = 10;
        p.damage = 5;
        gp.em.addEntity(police);
        p.attack();

        Assertions.assertEquals(5, police.health);

    }
}
