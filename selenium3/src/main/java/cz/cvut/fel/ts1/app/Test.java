package cz.cvut.fel.ts1.app;

import cz.cvut.fel.ts1.app.api.Route;
import cz.cvut.fel.ts1.app.selenium.MapyCzPage;
import cz.cvut.fel.ts1.app.selenium.MapyCzTSP;
import cz.cvut.fel.ts1.app.selenium.config.DriverFactory;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Test
{
    public static void main(String[] args) throws IOException {
        WebDriver driver = new DriverFactory().getDriver();
        MapyCzPage mp = new MapyCzPage(driver);
        mp.open();
        List<String> cities = new ArrayList<String>();
        cities.add("Praha");
        cities.add("Brno");
        cities.add("Pardubice");
        cities.add("Ostrava");

        Route r = mp.getRoute(cities);
        System.out.println(r.getDistance());
        System.out.println(r.getTime());
    }
}
