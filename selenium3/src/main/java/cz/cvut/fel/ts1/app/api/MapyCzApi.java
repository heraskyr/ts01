package cz.cvut.fel.ts1.app.api;

import cz.cvut.fel.ts1.app.selenium.MapyCzTSP;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MapyCzApi {

    @PostMapping(
            value = "/tsp",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    Route computeTSP(@RequestBody Request request) {
        Route r = new Route();
        List<String> addrs = request.getAddresses();
        MapyCzTSP mapyTSP = new MapyCzTSP(addrs);
        mapyTSP.solve();
        r = mapyTSP.solve();
        //r.setAddresses(addrs);

        return r;
    }
}
