package cz.cvut.fel.ts1.app.selenium;

import cz.cvut.fel.ts1.app.api.Route;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static java.lang.Thread.sleep;

public class MapyCzPage {
    private final WebDriver driver;
    private final String url = "https://en.mapy.cz/zakladni?planovani-trasy";

    public MapyCzPage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(url);
    }

    public void close() {
        driver.quit();
    }

    public long getDistance(String start, String end) {
        //enter start and end
            driver.findElement(By.xpath("//input[@placeholder='Enter a start']"))
                    .sendKeys(start+ Keys.TAB);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-item-point")));
        driver.findElement(By.xpath("//input[@placeholder='Enter an end']"))
                .sendKeys(end+ Keys.TAB);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".highligh-time")));
        //get distance
        String[] distance = driver.findElement(By.cssSelector(".highligh-time > .distance"))
                .getText().split(" ");
        //reload
        driver.findElement(By.xpath("//button[@data-log-text='stateswitch-route']")).click();

        //parse distance
        long d;
        if(distance[1].equals("km"))
            d = (long)(Float.parseFloat(distance[0]) * 1000);
        else //meters
            d = Long.parseLong(distance[0]);
        return d;
    }

    public Route getRoute(List<String> cities) {

        driver.findElement(By.cssSelector(".scmp_Button")).click();
        try {
            sleep(1000);//aaa
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.cssSelector(".scmp_Button")).click();
        Route r = new Route();
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-item-empty")));
        driver.findElement(By.xpath("//input[@placeholder='Enter a start']"))
                .sendKeys(cities.get(0)+ Keys.TAB);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-item-point")));
        driver.findElement(By.xpath("//input[@placeholder='Enter an end']"))
                .sendKeys(cities.get(0)+ Keys.TAB);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-item-plus > .plus")));


        for (int i = cities.size()-2; i > 0 ; i--)
        {
            new WebDriverWait(driver, 10)
                    .until(driver -> driver.findElement(By.cssSelector(".route-item-plus > .plus")));

            System.out.println(cities.get(i));
            driver.findElement(By.cssSelector(".route-item-plus > .plus")).click();
            driver.findElement(By.xpath("//input[@placeholder='Enter a POI']"))
                        .sendKeys(cities.get(i)+ Keys.TAB);
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        String distance = driver.findElement(By.cssSelector(".highligh-time > .distance")).getText();
        String time = driver.findElement(By.cssSelector(".highligh-time > .time")).getText();

        driver.findElement(By.xpath("//div[@title=\'Share\']")).click();


        r.setUrl(driver.getCurrentUrl());
        System.out.println(r.getUrl());
        r.setDistance(distance);
        r.setTime(time);
        return r;
    }
}
