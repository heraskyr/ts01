package cz.cvut.fel.ts1.selenium;

import cz.cvut.fel.ts1.selenium.config.DriverFactory;
import groovy.lang.Tuple;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


import javax.swing.text.html.parser.Element;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SeleniumTest
{
    private static WebDriver driver;

    @BeforeAll
    public static void getDriver() throws IOException
    {
        driver = new DriverFactory().getDriver();
    }

    @Test
    public void test1()
    {
        driver.get("https://flight-order.herokuapp.com");
        driver.findElement(By.id("flight_form_firstName")).sendKeys("aaa");
        Select selectDestination = new Select(driver.findElement(By.id("flight_form_destination")));
        selectDestination.selectByVisibleText("London");
        assertEquals("London", selectDestination.getFirstSelectedOption().getText());

        driver.findElement(By.id("flight_form_submit")).click();
    }

    @Test
    public void test_pruchodu()
    {
        driver.get("https://flight-order.herokuapp.com");
        String name = "name";
        String surname = "surname";
        String date = "01022000";
        String email = "email";
        driver.findElement(By.id("flight_form_firstName")).sendKeys(name);
        driver.findElement(By.id("flight_form_lastName")).sendKeys(surname);
        driver.findElement(By.id("flight_form_email")).sendKeys(email);
        driver.findElement(By.id("flight_form_birthDate")).click();

        driver.findElement(By.id("flight_form_birthDate")).sendKeys(date);
        //driver.findElement(By.id("flight_form_submit")).click();

    }

    @Test
    void testMap() throws Exception
    {
        driver.get("https://en.mapy.cz/");
        driver.findElement(By.xpath("//button[@class = 'icon route']")).click();
        driver.findElement(By.xpath("//input[@placeholder = 'Enter a start']")).sendKeys("Praha\n");

        WebElement firstresult = new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-item-point")));
        driver.findElement(By.xpath("//input[@placeholder = 'Enter an end']")).sendKeys("Pardubice\n");
        WebElement firstresult2 = new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-summary")));

        String time = driver.findElement(By.cssSelector(".alt0 > .time")).getText();
        String dist = driver.findElement(By.cssSelector(".alt0 > .distance")).getText();
        System.out.println(time);
        System.out.println(dist);
    }

    @Test
    public void test11()
    {
        DirectionsPage page = new DirectionsPage();
        page.setDriver(driver);
        page.open();
        Result res = page.getTimeAndAistance("Praha", "Pardubice");
    }

    @AfterAll
    public static void stopDriver()
    {
        //driver.quit();
    }
}
