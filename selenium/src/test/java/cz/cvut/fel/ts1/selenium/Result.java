package cz.cvut.fel.ts1.selenium;

public class Result
{
    String Start;
    String End;
    String distance;
    String time;

    public Result(String start, String end, String distance, String time) {
        Start = start;
        End = end;
        this.distance = distance;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Result{" +
                "Start='" + Start + '\'' +
                ", End='" + End + '\'' +
                ", distance='" + distance + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
