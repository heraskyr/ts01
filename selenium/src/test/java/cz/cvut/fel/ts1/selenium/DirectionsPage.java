package cz.cvut.fel.ts1.selenium;

import groovy.lang.Tuple;

import lombok.Getter;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.xml.xpath.XPath;

@Getter
@DefaultUrl("https://en.mapy.cz")
public class DirectionsPage extends PageObject
{
    @FindBy(xpath = "//input[@placeholder='Enter a start'" )
    WebElement startInput;

    @FindBy(xpath = "//input[@placeholder='Enter an end'" )
    WebElement endInput;

    @FindBy(css = ".route-item-point")
    WebElement inputDiv;

    @FindBy(css = ".highlight-time")
    WebElement timeDiv;

    @FindBy(css = ".highlight-time > .time")
    WebElement time;

    @FindBy(css = ".highlight-time > .distance")
    WebElement distance;

    public Result getTimeAndAistance(String start, String end)
    {
        startInput.sendKeys(start + Keys.TAB);

        new WebDriverWait(getDriver(), 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-item-point")));
        endInput.sendKeys(end + Keys.TAB);
        new WebDriverWait(getDriver(), 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-summary")));

        String t = time.getText().split(" ")[0];
        String d = distance.getText().split(" ")[0];

        return new Result(start,end,t, d);
    }

}
