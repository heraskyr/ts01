package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class calculatorTest
{
    static calculator c;
    /**
     * Nepracuje pro csvsource
    private static Stream<Arguments> generateMethod()
    {
        return Stream.of(
                Arguments.of("1,2,3"),
                Arguments.of("2,3,5")
        )
    }
    */
    @BeforeAll
    public static void initVariable() {
        c = new calculator();
    }

    @Test
    @DisplayName("Return number 5")
    public void sum_1_2_3() {
        //ARRANGE
        calculator c = new calculator();

        //ACT
        int num = c.add(1,2);

        //ASSERT
        Assertions.assertEquals(3, num);
    }

    @Test
    @Order(1)
    public void subtract_3_2_1()
    {
        //ACT
        int num = c.subtract(3,2);

        //ASSERT
        Assertions.assertEquals(1, num);
    }

    @Test
    @Order(2)
    public void multiply_2_3_6() {
        //ACT
        int num = c.multiply(2,3);

        //ASSERT
        Assertions.assertEquals(6, num);
    }

    @Test
    public void divide_6_3_2() throws Exception {

        int num = c.divide(6, 2);
        Assertions.assertEquals(3,num);
    }

    @Test
    public void exceptionThrownTest_exceptionThrown_exception() {
        //ARRANGE


        Exception exception = Assertions.assertThrows(Exception.class, () -> c.divide(3,0));

        String expectedMessage = "Dont divite by 0";
        String actualMessage = exception.getMessage();

        // ASSERT 2
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest(name = "Add({0},{1}) = {2}")
    @CsvFileSource(resources = "/input.csv", numLinesToSkip = 1)
    public void additionTest(int a, int b, int sum)
    {
        calculator c = new calculator();
        //ACT
        int num = c.add(a,b);
        //ASSERT
        Assertions.assertEquals(sum, num);
    }


    private static List<String> array = new ArrayList<>();


    @ParameterizedTest
    @CsvSource(value = {"2,3,5","3,4,9"})
    public void additionTest2(int a, int b, int sum)
    {
        calculator c = new calculator();
        //ACT
        int num = c.add(a,b);
        //ASSERT
        Assertions.assertEquals(sum, num);
    }

    private static Stream<Arguments> providerMethod()
    {
        return Stream.of(
                Arguments.of(1,2,3),
                Arguments.of(2,3,5)
        );
    }


    @ParameterizedTest
    @MethodSource(value = "providerMethod")
    public void additionTest3(int a, int b, int sum)
    {
        calculator c = new calculator();
        //ACT
        int num = c.add(a,b);
        //ASSERT
        Assertions.assertEquals(sum, num);
    }

    @ParameterizedTest
    @ValueSource(ints = {2,-4,8,10})
    public void isEvenTest_positive(int a)
    {
        //arrange
        calculator c = new calculator();
        //add
        boolean res = c.is_even(a);
        //assert
        assertTrue(res);
    }
}
